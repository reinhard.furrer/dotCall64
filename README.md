# Public repository for `dotCall64` package


* CRAN package link: https://CRAN.R-project.org/package=dotCall64

* `dotCall64` was by-product of the 64-bit extension of `spam` to `spam64`.  
  For detailed examples, consult a recent source of the `spam` package. (E.g., https://cran.r-project.org/src/contrib/spam_2.8-0.tar.gz).   
  The function `spam::bandwidth()` illustrates the R side of calling `.C64()`.

* Yifan Liu has written a comprehensive and detailed tutorial, available at
  https://fortran-lang.discourse.group/t/extend-r-with-fortran/2386.

